/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Str,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { IHiddenFieldStringCondition } from "../interface";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:string`,
    alias: `${PACKAGE_NAME}-string`,
})
export class HiddenFieldStringCondition extends ConditionBlock<
    IHiddenFieldStringCondition
> {
    @condition
    compare(): boolean {
        const slot = this.valueOf<string>();

        if (slot) {
            const currentValue = this.props.ignoreCase
                ? Str.lowercase(slot.string)
                : slot.string;
            const expectedValue = this.props.ignoreCase
                ? Str.lowercase(this.parseVariables(this.props.value || ""))
                : this.parseVariables(this.props.value || "");

            switch (this.props.mode) {
                case "equals":
                    return currentValue === expectedValue;
                case "not-equals":
                    return currentValue !== expectedValue;
                case "contains":
                    return currentValue.indexOf(expectedValue) !== -1;
                case "not-contains":
                    return currentValue.indexOf(expectedValue) === -1;
                case "defined":
                    return currentValue !== "";
                case "undefined":
                    return currentValue === "";
                case "regex":
                    try {
                        const regex = this.props.regex || "";
                        const literalSignLeft = regex.indexOf("/");
                        const literalSignRight = regex.lastIndexOf("/");

                        return (
                            literalSignLeft === 0 &&
                            literalSignRight > literalSignLeft &&
                            new Function(
                                "value",
                                `return ${
                                    this.props.invert ? "!" : ""
                                }${regex}.test(value)`
                            )(slot.string)
                        );
                    } catch {
                        return false;
                    }
            }
        }

        return false;
    }
}

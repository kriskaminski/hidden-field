/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToNumber,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { IHiddenFieldNumberCondition } from "../interface";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:number`,
    alias: `${PACKAGE_NAME}-number`,
})
export class HiddenFieldNumberCondition extends ConditionBlock<
    IHiddenFieldNumberCondition
> {
    @condition
    compare(): boolean {
        const slot = this.valueOf<number>();

        if (slot && slot.hasValue) {
            switch (this.props.mode) {
                case "equal":
                    return slot.value === this.props.value;
                case "below":
                    return slot.value < castToNumber(this.props.value);
                case "above":
                    return slot.value > castToNumber(this.props.value);
                case "between":
                    return (
                        slot.value >= castToNumber(this.props.value) &&
                        slot.value <= castToNumber(this.props.to)
                    );
            }
        }

        return false;
    }
}

/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Slots,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { IHiddenFieldDateCondition } from "../interface";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:date`,
})
export class HiddenFieldDateCondition extends ConditionBlock<
    IHiddenFieldDateCondition
> {
    @condition
    verify(): boolean {
        const dateSlot = this.valueOf<number, Slots.Date>();

        if (dateSlot && dateSlot.hasValue) {
            switch (this.props.mode) {
                case "equal":
                    return (
                        dateSlot.slot.toValue(dateSlot.value) ===
                        dateSlot.slot.toValue(this.props.value)
                    );
                case "before":
                    return (
                        dateSlot.value < dateSlot.slot.toValue(this.props.value)
                    );
                case "after":
                    return (
                        dateSlot.value > dateSlot.slot.toValue(this.props.value)
                    );
                case "between":
                    return (
                        dateSlot.value >=
                            dateSlot.slot.toValue(this.props.value) &&
                        dateSlot.value <= dateSlot.slot.toValue(this.props.to)
                    );
            }
        }

        return false;
    }
}

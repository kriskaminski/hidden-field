/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { IHiddenFieldOrientationCondition } from "../interface";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:orientation`,
    alias: `${PACKAGE_NAME}-orientation`,
})
export class HiddenFieldOrientationCondition extends ConditionBlock<
    IHiddenFieldOrientationCondition
> {
    @condition
    compare(): boolean {
        const slot = this.valueOf<string>();

        return (slot && slot.value === this.props.orientation) || false;
    }
}

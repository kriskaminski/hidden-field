import { TFieldTypes } from "./types";

export interface IHiddenField {
    fieldType: TFieldTypes | string;
    fieldValue: string | undefined;
    fieldQueryStringParameter: string | undefined;
    fieldCookie: string | undefined;
    fieldLocalStorage: string | undefined;
    fieldSessionStorage: string | undefined;
    fieldVariable: string | undefined;
}

export type THiddenFieldStringConditionMode =
    | "equals"
    | "not-equals"
    | "contains"
    | "not-contains"
    | "defined"
    | "undefined"
    | "regex";

export interface IHiddenFieldStringCondition {
    mode: THiddenFieldStringConditionMode;
    value?: string;
    ignoreCase?: boolean;
    regex?: string;
    invert?: boolean;
}

export type THiddenFieldNumberConditionMode =
    | "equal"
    | "below"
    | "above"
    | "between";

export interface IHiddenFieldNumberCondition {
    mode: THiddenFieldNumberConditionMode;
    value?: number;
    to?: number;
}

export interface IHiddenFieldOrientationCondition {
    orientation: "landscape" | "portrait";
}

export type THiddenFieldDateConditionMode =
    | "equal"
    | "before"
    | "after"
    | "between";

export interface IHiddenFieldDateCondition {
    mode: THiddenFieldDateConditionMode;
    value?: number;
    to?: number;
}

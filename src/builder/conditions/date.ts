/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    DateTime as DateHelper,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { HiddenField } from "../";
import {
    IHiddenFieldDateCondition,
    THiddenFieldDateConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-date.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:date`,
    version: PACKAGE_VERSION,
    context: HiddenField,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Verify date");
    },
})
export class HiddenFieldDateCondition
    extends ConditionBlock
    implements IHiddenFieldDateCondition {
    @definition
    @affects("#name")
    mode: THiddenFieldDateConditionMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot as Slots.Date | undefined;

        if (slot) {
            const value =
                this.value &&
                L10n.locale.dateTimeShort(slot.toValue(this.value), true);

            switch (this.mode) {
                case "between":
                    const to =
                        this.to &&
                        L10n.locale.dateTimeShort(slot.toValue(this.to), true);

                    return `${value ? `\`${value}\`` : "\\_"} ≤ @${slot.id} ≤ ${
                        to ? `\`${to}\`` : "\\_"
                    }`;
                case "before":
                case "after":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "after"
                            ? pgettext("block:hidden-field", "after")
                            : this.mode === "before"
                            ? pgettext("block:hidden-field", "before")
                            : "="
                    } ${value ? `\`${value}\`` : "\\_"}`;
            }
        }

        return this.type.label;
    }

    static getToday(s: "begin" | "end"): number {
        const today = new Date();

        today.setUTCHours(0);
        today.setUTCMinutes(0);
        today.setUTCSeconds(0);
        today.setUTCMilliseconds(0);

        return today.getTime() + (s === "end" ? 86400000 - 1 : 0);
    }

    @editor
    defineEditor(): void {
        const dateTo = new Forms.DateTime(
            Forms.DateTime.bind(
                this,
                "to",
                undefined,
                HiddenFieldDateCondition.getToday("end")
            )
        )
            .zone("UTC")
            .features(
                Forms.DateTimeFeatures.Date |
                    Forms.DateTimeFeatures.Weekday |
                    Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
            )
            .years(new Date().getFullYear() - 25, new Date().getFullYear() + 25)
            .width("full")
            .required()
            .visible(this.mode === "between");

        this.editor.form({
            title: pgettext("block:hidden-field", "When date:"),
            controls: [
                new Forms.Radiobutton<THiddenFieldDateConditionMode>(
                    [
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:hidden-field", "Is before"),
                            value: "before",
                        },
                        {
                            label: pgettext("block:hidden-field", "Is after"),
                            value: "after",
                        },
                        {
                            label: pgettext("block:hidden-field", "Is between"),
                            value: "between",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldDateConditionMode>
                    ) => {
                        dateTo.visible(mode.value === "between");
                    }
                ),
                new Forms.Group([
                    new Forms.DateTime(
                        Forms.DateTime.bind(
                            this,
                            "value",
                            undefined,
                            HiddenFieldDateCondition.getToday("begin")
                        )
                    )
                        .zone("UTC")
                        .features(
                            Forms.DateTimeFeatures.Date |
                                Forms.DateTimeFeatures.Weekday |
                                Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                        )
                        .years(
                            new Date().getFullYear() - 25,
                            new Date().getFullYear() + 25
                        )
                        .width("full")
                        .required(),
                    dateTo,
                ]),
            ],
        });
    }
}

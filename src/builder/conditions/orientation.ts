/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { HiddenField } from "../";
import { IHiddenFieldOrientationCondition } from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-orientation.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:orientation`,
    alias: `${PACKAGE_NAME}-orientation`,
    version: PACKAGE_VERSION,
    context: HiddenField,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Orientation");
    },
})
export class HiddenFieldOrientationCondition
    extends ConditionBlock
    implements IHiddenFieldOrientationCondition {
    @definition
    @affects("#name")
    orientation: "landscape" | "portrait" = "landscape";

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            return `@${this.slot.id} = ${`\`${this.slot.toString(
                this.orientation
            )}\``}`;
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            controls: [
                new Forms.Radiobutton<"landscape" | "portrait">(
                    [
                        {
                            label: pgettext("block:hidden-field", "Landscape"),
                            value: "landscape",
                        },
                        {
                            label: pgettext("block:hidden-field", "Portrait"),
                            value: "portrait",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "orientation", "landscape")
                ),
            ],
        });
    }
}

/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    slotInsertAction,
    tripetto,
} from "tripetto";
import { HiddenField } from "../";
import {
    IHiddenFieldStringCondition,
    THiddenFieldStringConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-string.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:string`,
    alias: `${PACKAGE_NAME}-string`,
    version: PACKAGE_VERSION,
    context: HiddenField,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Compare value");
    },
})
export class HiddenFieldStringCondition
    extends ConditionBlock
    implements IHiddenFieldStringCondition {
    @definition
    @affects("#name")
    mode: THiddenFieldStringConditionMode = "equals";

    @definition
    @affects("#name")
    value?: string;

    @definition
    ignoreCase?: boolean;

    @definition
    regex?: string;

    @definition
    invert?: boolean;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "equals":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "matches"
                    )} ${this.value ? `\`${this.value}\`` : "\\_"}`;
                case "not-equals":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "does not match"
                    )} ${this.value ? `\`${this.value}\`` : "\\_"}`;
                case "contains":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "contains"
                    )} ${this.value ? `\`${this.value}\`` : "\\_"}`;
                case "not-contains":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "does not contain"
                    )} ${this.value ? `\`${this.value}\`` : "\\_"}`;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "not specified"
                    )}`;
                case "regex":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "regular expression"
                    )}`;
            }
        }

        return this.value || this.type.label;
    }

    @editor
    defineEditor(): void {
        const value = new Forms.Group([
            new Forms.Text(
                "singleline",
                Forms.Text.bind(this, "value", undefined, "")
            )
                .action("@", slotInsertAction(this, "exclude"))
                .autoFocus(),
            new Forms.Checkbox(
                pgettext("block:hidden-field", "Ignore case"),
                Forms.Checkbox.bind(this, "ignoreCase", undefined, true)
            ),
        ]).visible(
            this.mode !== "defined" &&
                this.mode !== "undefined" &&
                this.mode !== "regex"
        );
        const regex = new Forms.Group([
            new Forms.Text(
                "singleline",
                Forms.Text.bind(this, "regex", undefined, "")
            )
                .placeholder(
                    pgettext(
                        "block:hidden-field",
                        "Regex literal (for example /ab+c/)"
                    )
                )
                .autoFocus()
                .autoValidate((regexControl) => {
                    if (!regexControl.value) {
                        return "unknown";
                    }

                    try {
                        const literalSignLeft = regexControl.value.indexOf("/");
                        const literalSignRight = regexControl.value.lastIndexOf(
                            "/"
                        );

                        return literalSignLeft === 0 &&
                            literalSignRight > literalSignLeft &&
                            new Function("", `return ${regexControl.value}`)()
                            ? "pass"
                            : "fail";
                    } catch {
                        return "fail";
                    }
                }),
            new Forms.Checkbox(
                pgettext("block:hidden-field", "Invert regular expression"),
                Forms.Checkbox.bind(this, "invert", undefined, false)
            ),
        ]).visible(this.mode === "regex");

        this.editor.form({
            title: pgettext("block:hidden-field", "When value:"),
            controls: [
                new Forms.Radiobutton<THiddenFieldStringConditionMode>(
                    [
                        {
                            label: pgettext("block:hidden-field", "Matches"),
                            value: "equals",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Does not match"
                            ),
                            value: "not-equals",
                        },
                        {
                            label: pgettext("block:hidden-field", "Contains"),
                            value: "contains",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Does not contain"
                            ),
                            value: "not-contains",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is specified"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is not specified"
                            ),
                            value: "undefined",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Satisfies regular expression"
                            ),
                            value: "regex",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equals")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldStringConditionMode>
                    ) => {
                        value.visible(
                            mode.value !== "defined" &&
                                mode.value !== "undefined" &&
                                mode.value !== "regex"
                        );

                        regex.visible(mode.value === "regex");
                    }
                ),
                value,
                regex,
            ],
        });
    }
}

/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { HiddenField } from "../";
import {
    IHiddenFieldNumberCondition,
    THiddenFieldNumberConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-number.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:number`,
    alias: `${PACKAGE_NAME}-number`,
    version: PACKAGE_VERSION,
    context: HiddenField,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Compare value");
    },
})
export class HiddenFieldNumberCondition
    extends ConditionBlock
    implements IHiddenFieldNumberCondition {
    @definition
    @affects("#name")
    mode: THiddenFieldNumberConditionMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            return this.mode === "between"
                ? `\`${this.slot.toString(this.value)}\` ≤ @${
                      this.slot.id
                  } ≤ \`${this.slot.toString(this.to)}\``
                : `@${this.slot.id} ${
                      this.mode === "above"
                          ? ">"
                          : this.mode === "below"
                          ? "<"
                          : "="
                  } ${`\`${this.slot.toString(this.value)}\``}`;
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        const valueTo = new Forms.Numeric(
            Forms.Numeric.bind(this, "to", undefined, 0)
        ).visible(this.mode === "between");

        this.editor.form({
            controls: [
                new Forms.Radiobutton<THiddenFieldNumberConditionMode>(
                    [
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is lower than"
                            ),
                            value: "below",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Is higher than"
                            ),
                            value: "above",
                        },
                        {
                            label: pgettext("block:hidden-field", "Is between"),
                            value: "between",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldNumberConditionMode>
                    ) => {
                        valueTo.visible(mode.value === "between");
                    }
                ),
                new Forms.Numeric(
                    Forms.Numeric.bind(this, "value", undefined, 0)
                ).autoFocus(),
                valueTo,
            ],
        });
    }
}

/** Dependencies */
import { findFirst, pgettext } from "tripetto";
import { TFieldTypes } from "../runner/types";
import { HiddenField } from ".";

export function getTypeName(type: TFieldTypes | string): string {
    switch (type) {
        case "value":
            return pgettext("block:hidden-field", "Fixed value");
        case "uuid":
            return pgettext("block:hidden-field", "Random value");
        case "timestamp":
            return pgettext("block:hidden-field", "Timestamp");
        case "variable":
            return pgettext("block:hidden-field", "Global JavaScript variable");
        case "language":
            return pgettext("block:hidden-field", "User language");
        case "querystring":
            return pgettext("block:hidden-field", "Query string");
        case "cookie":
            return pgettext("block:hidden-field", "Cookie");
        case "localStorage":
            return pgettext("block:hidden-field", "Local storage");
        case "sessionStorage":
            return pgettext("block:hidden-field", "Session storage");
        case "user-agent":
            return pgettext("block:hidden-field", "User-Agent string");
        case "title":
            return pgettext("block:hidden-field", "Page title");
        case "url":
            return pgettext("block:hidden-field", "Page URL");
        case "referrer":
            return pgettext("block:hidden-field", "Page referrer");
        case "screenOrientation":
            return pgettext("block:hidden-field", "Screen orientation");
        case "screenWidth":
            return pgettext("block:hidden-field", "Screen width");
        case "screenHeight":
            return pgettext("block:hidden-field", "Screen height");
        case "devicePixelRatio":
            return pgettext("block:hidden-field", "Screen pixel ratio");
        default:
            if (
                type.indexOf("custom-variable:") === 0 &&
                HiddenField.customVariables
            ) {
                const customVariable = findFirst(
                    HiddenField.customVariables.variables,
                    (variable) =>
                        variable.name === type.replace("custom-variable:", "")
                );

                if (customVariable && customVariable.description) {
                    return customVariable.description;
                }
            }

            return pgettext("block:hidden-field", "Unknown");
    }
}

export function getTypeDescription(type: TFieldTypes | string): string {
    switch (type) {
        case "value":
            return pgettext(
                "block:hidden-field",
                "Defines a fixed value for the hidden field."
            );
        case "uuid":
            return pgettext(
                "block:hidden-field",
                "Retrieves a random string value."
            );
        case "timestamp":
            return pgettext(
                "block:hidden-field",
                "Retrieves the current date and time."
            );
        case "variable":
            return pgettext(
                "block:hidden-field",
                "Retrieves a JavaScript global variable (only string values are supported)."
            );
        case "language":
            return pgettext(
                "block:hidden-field",
                "Retrieves the preferred language of the user."
            );
        case "querystring":
            return pgettext(
                "block:hidden-field",
                "Retrieves the complete query string or a specific query string parameter value."
            );
        case "cookie":
            return pgettext(
                "block:hidden-field",
                "Read one or all cookies associated with the document."
            );
        case "localStorage":
            return pgettext(
                "block:hidden-field",
                "Retrieves an item from the browser local storage (localStorage)."
            );
        case "sessionStorage":
            return pgettext(
                "block:hidden-field",
                "Retrieves an item from the browser session storage (sessionStorage)."
            );
        case "user-agent":
            return pgettext(
                "block:hidden-field",
                "Retrieves the browser User-Agent string."
            );
        case "title":
            return pgettext(
                "block:hidden-field",
                "Retrieves the title of the page."
            );
        case "url":
            return pgettext(
                "block:hidden-field",
                "Retrieves the URL of the page."
            );
        case "referrer":
            return pgettext(
                "block:hidden-field",
                "Retrieves the URL of the page that linked to this page."
            );
        case "screenOrientation":
            return pgettext(
                "block:hidden-field",
                "Retrieves the current orientation of the screen (landscape or portrait)."
            );
        case "screenWidth":
            return pgettext(
                "block:hidden-field",
                "Retrieves the width of the screen in pixels."
            );
        case "screenHeight":
            return pgettext(
                "block:hidden-field",
                "Retrieves the height of the screen in pixels."
            );
        case "devicePixelRatio":
            return pgettext(
                "block:hidden-field",
                "Retrieves the ratio of the resolution in physical pixels to the resolution in CSS pixels for the current display device."
            );
        default:
            if (
                type.indexOf("custom-variable:") === 0 &&
                HiddenField.customVariables
            ) {
                const customVariable = findFirst(
                    HiddenField.customVariables.variables,
                    (variable) =>
                        variable.name === type.replace("custom-variable:", "")
                );

                if (customVariable && customVariable.description) {
                    return pgettext(
                        "block:hidden-field",
                        "Retrieves the value of *%1*.",
                        customVariable.description
                    );
                }
            }

            return pgettext("block:hidden-field", "Invalid type.");
    }
}
